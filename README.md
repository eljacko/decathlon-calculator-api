## Building

Gradle is used for building the project and compiling a distribution archive. 
To build everything:

    ./gradlew build

For running on your computer use, execute subproject task:

    ./gradlew clean build :public-api:bootRun
    
To run the application without first building an archive use the bootRun task:
    
    ./gradlew :public-api:bootRun


To build all spring-boot jars:

    ./gradlew clean bootJar
    
By default, application uses 8080 port, to change port put in application.properties:

	server.port=8084

## Configuration
There should be 2 configurations files to run an application on a developer's computer:

	application.properties (additional)
	log4j2.xml
	
#### Additional `application.properties` file
Default values for the Decathlon Calculator Api application are provided in a `application.properties` file in the location `public-api/src/main/resources`. This file is called as **default** `application.properties` file. To run Decathlon Calculator API application you need to create an additional `application.properties` file where you define environment values. This file is called as **additional** `application.properties` file. A template for the additional file is located in `public-api/` directory as `application.properties.additional.sample`, there are the minimum values that you need to run the application. Put your additional `application.properties` file to the `public-api` module root directory to load configuration properties automatically. If there is need to change values defined in the default file, then you can do it by putting them to your additional `application.properties` file. All values are defined in the additional are override values from the the default `application.properties` file.

#### `log4j2.xml`
For logging, you need to create a file `log4j2.xml`. A template for the file is located in `public-api/src/main/resources` `log4j2.xml.sample` Put the created file to the `public-api` module root directory or to `public-api/src/main/resources` to load logging configuration automatically.

### Creating databases
Applications need 2 separate databases to run, database creation script `CreateDatabaseSQL.sql` is located in `data-model/resources` 

    CREATE DATABASE decathlon_calculator_db;
    CREATE ROLE industry62 WITH LOGIN PASSWORD 'ajutine123';
    GRANT ALL PRIVILEGES ON DATABASE decathlon_calculator_db to industry62;
    