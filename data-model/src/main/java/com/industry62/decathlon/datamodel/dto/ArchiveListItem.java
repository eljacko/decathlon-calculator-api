package com.industry62.decathlon.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ArchiveListItem {

    private Long id;
    private String firstName;
    private String lastName;
    private Integer totalPoints;
    private Set<ArchiveListItemEvent> events;
    private String memo;

    public ArchiveListItem(final Long id, final String firstName, final String lastName,
                       final Integer totalPoints, final Set<ArchiveListItemEvent> events,
                       final String memo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalPoints = totalPoints;
        this.events = events;
        this.memo = memo;
    }
}
