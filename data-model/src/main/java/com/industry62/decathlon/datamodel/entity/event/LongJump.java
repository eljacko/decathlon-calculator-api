package com.industry62.decathlon.datamodel.entity.event;

import com.industry62.decathlon.datamodel.constant.DecathlonEvent;
import com.industry62.decathlon.datamodel.constant.DecathlonEventTypeDiscriminator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@ToString(callSuper = true)
@DiscriminatorValue(DecathlonEventTypeDiscriminator.LONG_JUMP)
public class LongJump extends Event {

    private static final long serialVersionUID = -8863536194903172902L;

    public LongJump() {
        super();
        this.setEventTypeId(DecathlonEvent.LONG_JUMP.getId());
    }
}
