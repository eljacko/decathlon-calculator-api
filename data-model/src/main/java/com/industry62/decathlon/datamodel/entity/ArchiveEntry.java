package com.industry62.decathlon.datamodel.entity;

import com.industry62.decathlon.datamodel.constant.FieldsLength;
import com.industry62.decathlon.datamodel.constant.ValidationMessages;
import com.industry62.decathlon.datamodel.entity.base.BaseEntity;
import com.industry62.decathlon.datamodel.entity.event.Event;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "archive")
@DynamicUpdate
@DynamicInsert
@SuppressWarnings({ "checkstyle:MemberName", "checkstyle:MethodName",
        "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.industry62.decathlon.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "archive_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class ArchiveEntry extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String firstName;
    private String lastName;
    private Integer totalPoints;
    private Set<Event> events;
    private String memo;

    public ArchiveEntry() {
        super();
    }

    @Size(max = FieldsLength.ARCHIVE_ENTRY_FIRST_NAME,
            message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.ARCHIVE_ENTRY_FIRST_NAME)
    public String getFirstName() {
        return firstName;
    }

    @Size(max = FieldsLength.ARCHIVE_ENTRY_LAST_NAME,
            message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.ARCHIVE_ENTRY_LAST_NAME)
    public String getLastName() {
        return lastName;
    }

    @Column(name = "total_points")
    public Integer getTotalPoints() {
        return totalPoints;
    }

    @Size(max = FieldsLength.ARCHIVE_ENTRY_MEMO, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.ARCHIVE_ENTRY_MEMO)
    public String getMemo() {
        return memo;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "archiveEntry")
    public Set<Event> getEvents() {
        return events;
    }

    @Transient
    public void setEvents(final Set<Event> events) {
        this.events = events;
    }
}
