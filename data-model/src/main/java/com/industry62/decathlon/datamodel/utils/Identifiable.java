package com.industry62.decathlon.datamodel.utils;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
}
