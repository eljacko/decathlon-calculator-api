package com.industry62.decathlon.datamodel.entity.event;

import com.industry62.decathlon.datamodel.constant.DecathlonEvent;
import com.industry62.decathlon.datamodel.constant.DecathlonEventTypeDiscriminator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@ToString(callSuper = true)
@DiscriminatorValue(DecathlonEventTypeDiscriminator.DISCUS_THROW)
public class DiscusThrow extends Event {

    private static final long serialVersionUID = -8863536194903172902L;

    public DiscusThrow() {
        super();
        this.setEventTypeId(DecathlonEvent.DISCUS_THROW.getId());
    }
}
