package com.industry62.decathlon.datamodel.repository.archive;

import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import com.industry62.decathlon.datamodel.dto.PageResultWrapper;
import com.industry62.decathlon.datamodel.dto.search.ArchiveSearchTerms;

public interface CustomizedArchiveRepository {
    PageResultWrapper<ArchiveListItem> getArchiveList(ArchiveSearchTerms terms);
}
