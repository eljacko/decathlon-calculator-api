package com.industry62.decathlon.datamodel.repository;

import com.industry62.decathlon.datamodel.entity.EventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Integer> {

    // @formatter:off
    @Query("SELECT e FROM EventType e "
            + " WHERE e.id = :id"
    )
    // @formatter:on
    EventType findById(@Param("id") int id);

}
