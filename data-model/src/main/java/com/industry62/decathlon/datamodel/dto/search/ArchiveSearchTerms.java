package com.industry62.decathlon.datamodel.dto.search;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@ToString
@Getter
@EqualsAndHashCode
public class ArchiveSearchTerms {
    private String firstName;
    private String lastName;
}
