package com.industry62.decathlon.datamodel.entity;

import com.industry62.decathlon.datamodel.constant.FieldsLength;
import com.industry62.decathlon.datamodel.constant.ValidationMessages;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The persistent class for the event_type database table.
 *
 */
@Entity
@Table(name = "event_type")
@Setter
@ToString
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class EventType implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private Boolean timed;

    public EventType() {
    }

    @Id
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    @Size(max = FieldsLength.EVENT_TYPE_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(unique = true, length = FieldsLength.EVENT_TYPE_NAME)
    public String getName() {
        return name;
    }

    public boolean isTimed() {
        return timed;
    }


    @SuppressWarnings({ "checkstyle:AvoidInlineConditionals" })
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getId() == null) ? 0 : this.getId().hashCode());
        result = prime * result + getClass().hashCode();
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof EventType)) {
            return false;
        }
        EventType other = (EventType) obj;
        if (this.getId() == null || other.getId() == null) {
            return false;
        } else if (!this.getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

}
