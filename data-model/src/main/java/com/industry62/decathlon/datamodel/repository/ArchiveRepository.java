package com.industry62.decathlon.datamodel.repository;

import com.industry62.decathlon.datamodel.entity.ArchiveEntry;
import com.industry62.decathlon.datamodel.repository.archive.CustomizedArchiveRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArchiveRepository extends JpaRepository<ArchiveEntry, Long>,
        CustomizedArchiveRepository {

}
