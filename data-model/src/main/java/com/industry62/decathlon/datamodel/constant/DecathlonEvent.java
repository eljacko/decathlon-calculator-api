package com.industry62.decathlon.datamodel.constant;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.fasterxml.jackson.annotation.JsonValue;
import com.industry62.decathlon.datamodel.constant.base.Event;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public enum DecathlonEvent implements Event {
    // a, b and c values from https://en.wikipedia.org/wiki/Decathlon
    TRACK_100M(1, "TRACK_100M",
            new BigDecimal("25.4347"), new BigDecimal("18"), new BigDecimal("1.8")),
    LONG_JUMP(2, "LONG_JUMP",
            new BigDecimal("0.14354"), new BigDecimal("220"), new BigDecimal("1.4")),
    SHOT_PUT(3, "SHOT_PUT",
            new BigDecimal("51.39"), new BigDecimal("1.5"), new BigDecimal("1.05")),
    HIGH_JUMP(4, "HIGH_JUMP",
            new BigDecimal("0.8465"), new BigDecimal("75"), new BigDecimal("1.42")),
    TRACK_400M(5, "TRACK_400M",
            new BigDecimal("1.53775"), new BigDecimal("82"), new BigDecimal("1.81")),
    TRACK_110M_HURDLES(6, "TRACK_110M_HURDLES",
            new BigDecimal("5.74352"), new BigDecimal("28.5"), new BigDecimal("1.92")),
    DISCUS_THROW(7, "DISCUS_THROW",
            new BigDecimal("12.91"), new BigDecimal("4"), new BigDecimal("1.1")),
    POLE_VAULT(8, "POLE_VAULT",
            new BigDecimal("0.2797"), new BigDecimal("100"), new BigDecimal("1.35")),
    JAVELIN_THROW(9, "JAVELIN_THROW",
            new BigDecimal("10.14"), new BigDecimal("7"), new BigDecimal("1.08")),
    TRACK_1500M(10, "TRACK_1500M",
            new BigDecimal("0.03768"), new BigDecimal("480"), new BigDecimal("1.85"));

    private final int id;
    private final String name;
    private final BigDecimal a;
    private final BigDecimal b;
    private final BigDecimal c;

    DecathlonEvent(final Integer id, final String name,
                   final BigDecimal a, final BigDecimal b, final BigDecimal c) {
        this.id = id;
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @JsonValue
    @Override
    public final int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getA() {
        return a;
    }

    @Override
    public BigDecimal getB() {
        return b;
    }

    @Override
    public BigDecimal getC() {
        return c;
    }

    public boolean isEvent(final String event) {
        return this.name.equals(event);
    }

    public static DecathlonEvent getEnum(final String value) {
        for (DecathlonEvent e : values()) {
            if (e.getName().equalsIgnoreCase(value)) {
                return e;
            }
        }
        throw new IllegalArgumentException("Unknown event: " + value + ".");
    }

    /**
     * Returns type names for Events where result is measured with meters
     *
     * @return names of Events where result is measured with meters
     */
    public static Set<String> getDistanceEvents() {
        return Arrays
                .stream(new DecathlonEvent[]{SHOT_PUT, DISCUS_THROW, JAVELIN_THROW})
                .map(DecathlonEvent::getName).collect(Collectors.toSet());
    }

    /**
     * Returns type names for Events where result is measured by time
     *
     * @return names of Events where result is measured in time units
     */
    public static Set<String> getTimedEvents() {
        return Arrays
                .stream(new DecathlonEvent[]{TRACK_100M, TRACK_400M, TRACK_110M_HURDLES,
                        TRACK_1500M})
                .map(DecathlonEvent::getName).collect(Collectors.toSet());
    }

    /**
     * Returns type names for Events where result is measured with cm
     *
     * @return names of Events where result is measured with cm
     */
    public static Set<String> getHeightEvents() {
        return Arrays
                .stream(new DecathlonEvent[]{LONG_JUMP, HIGH_JUMP, POLE_VAULT})
                .map(DecathlonEvent::getName).collect(Collectors.toSet());
    }

    public static boolean isTimedEvent(final String eventName) {
        for (String event: getTimedEvents()) {
            if (eventName.equalsIgnoreCase(event)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isHeightEvent(final String eventName) {
        for (String event: getHeightEvents()) {
            if (eventName.equalsIgnoreCase(event)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDistanceEvent(final String eventName) {
        for (String event: getDistanceEvents()) {
            if (eventName.equalsIgnoreCase(event)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCorrect(final String name) {
        return Arrays.stream(DecathlonEvent.values()).anyMatch(e -> e.getName().equals(name));
    }

    @Override
    public Integer getPoints(final BigDecimal performance) {
        if (isTimedEvent(name)) {
            try {
                return getA().multiply(
                        BigDecimalMath.pow(
                                getB().subtract(performance),
                                getC(),
                                MathContext.DECIMAL64))
                        .intValue();
            } catch (ArithmeticException ex) {
                return 0;
            }
        } else {
            BigDecimal calcPerformance = performance;
            if (isHeightEvent(name)) {
                calcPerformance = performance.multiply(new BigDecimal("100"));
            }
            try {
                return getA().multiply(
                        BigDecimalMath.pow(
                                calcPerformance.subtract(getB()),
                                getC(),
                                MathContext.DECIMAL64))
                        .intValue();
            } catch (ArithmeticException ex) {
                return 0;
            }
        }
    }
    // Points = INT(A(B — P)C) for track events (faster time produces a higher score)
    // Points = INT(A(P — B)C) for field events (greater distance or height produces a higher score)

    public static Set<String> getAllDecathlonEvents() {
        return Arrays.stream(DecathlonEvent.values())
                .map(DecathlonEvent::name).collect(Collectors.toSet());
    }
}
