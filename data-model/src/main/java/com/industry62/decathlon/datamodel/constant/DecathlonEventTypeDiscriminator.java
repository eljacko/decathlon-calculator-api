package com.industry62.decathlon.datamodel.constant;

public final class DecathlonEventTypeDiscriminator {
    public static final String TRACK_100M = "1";
    public static final String LONG_JUMP = "2";
    public static final String SHOT_PUT = "3";
    public static final String HIGH_JUMP = "4";
    public static final String TRACK_400M = "5";
    public static final String TRACK_110M_HURDLES = "6";
    public static final String DISCUS_THROW = "7";
    public static final String POLE_VAULT = "8";
    public static final String JAVELIN_THROW = "9";
    public static final String TRACK_1500M = "10";

    private DecathlonEventTypeDiscriminator() {
    }
}
