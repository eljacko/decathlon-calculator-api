package com.industry62.decathlon.datamodel.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PageResultWrapper<T> {
    private List<T> results;
}
