package com.industry62.decathlon.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ArchiveListItemEvent {

    private String eventName;
    private String result;
    private Integer points;
    private Boolean timed;

    public ArchiveListItemEvent(final String eventName, final String result,
                                final Integer points, final boolean timed) {
        this.eventName = eventName;
        this.result = result;
        this.points = points;
        this.timed = timed;
    }
}
