package com.industry62.decathlon.datamodel.constant;

public final class FieldsLength {

    public static final int EVENT_TYPE_NAME = 32;
    public static final int ARCHIVE_ENTRY_FIRST_NAME = 64;
    public static final int ARCHIVE_ENTRY_LAST_NAME = 64;
    public static final int ARCHIVE_ENTRY_MEMO = 255;


    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
