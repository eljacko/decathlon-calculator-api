package com.industry62.decathlon.datamodel.entity.event;

import com.industry62.decathlon.datamodel.constant.ValidationMessages;
import com.industry62.decathlon.datamodel.entity.ArchiveEntry;
import com.industry62.decathlon.datamodel.entity.EventType;
import com.industry62.decathlon.datamodel.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Entity
@Table(name = "event")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_id", discriminatorType = DiscriminatorType.INTEGER)
@DynamicUpdate
@DynamicInsert
@Setter
@SuppressWarnings({ "checkstyle:MemberName", "checkstyle:MethodName",
        "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.industry62.decathlon.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "event_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public abstract class Event extends BaseEntity {
    private static final long serialVersionUID = 1549505726733603361L;

    /**
     * Reference to the archive entry
     */
    @ToString.Exclude
    private ArchiveEntry archiveEntry;
    private EventType type;
    private Integer points;
    private BigDecimal result;

    public Event() {
        super();
    }

    @Column(name = "points")
    public Integer getPoints() {
        return points;
    }

    @Column(name = "result")
    public BigDecimal getResult() {
        return result;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "archive_id", nullable = false, updatable = false)
    public ArchiveEntry getArchiveEntry() {
        return archiveEntry;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", insertable = false, updatable = false, nullable = false)
    public EventType getType() {
        return type;
    }

    public void setType(final EventType type) {
        this.type = type;
    }

    @Transient
    public void setEventTypeId(final int typeId) {
        this.setType(new EventType());
        this.getType().setId(typeId);
    }
}
