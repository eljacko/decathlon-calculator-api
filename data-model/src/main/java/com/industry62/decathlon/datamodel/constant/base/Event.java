package com.industry62.decathlon.datamodel.constant.base;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public interface Event {
    int getId();
    String getName();
    BigDecimal getA();
    BigDecimal getB();
    BigDecimal getC();
    Integer getPoints(@NotNull BigDecimal performance);
}
