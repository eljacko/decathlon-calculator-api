package com.industry62.decathlon.datamodel.repository;

import com.industry62.decathlon.datamodel.entity.event.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}
