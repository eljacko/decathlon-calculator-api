package com.industry62.decathlon.datamodel.repository.archive;

import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import com.industry62.decathlon.datamodel.dto.ArchiveListItemEvent;
import com.industry62.decathlon.datamodel.dto.PageResultWrapper;
import com.industry62.decathlon.datamodel.dto.search.ArchiveSearchTerms;
import com.industry62.decathlon.datamodel.entity.ArchiveEntry;
import com.industry62.decathlon.datamodel.entity.ArchiveEntry_;
import com.industry62.decathlon.datamodel.entity.event.Event;
import com.industry62.decathlon.datamodel.utils.StringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Getter
@Setter
@Slf4j
@SuppressWarnings("checkstyle:designforextension")
public class CustomizedArchiveRepositoryImpl implements CustomizedArchiveRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public PageResultWrapper<ArchiveListItem> getArchiveList(final ArchiveSearchTerms terms) {
        PageResultWrapper<ArchiveListItem> result = new PageResultWrapper<>();
        result.setResults(new ArrayList<>());

        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<ArchiveEntry> query = em.getCriteriaBuilder().createQuery(ArchiveEntry.class);
        Root<ArchiveEntry> archiveTable = query.from(ArchiveEntry.class);
        archiveTable.join(ArchiveEntry_.EVENTS, JoinType.LEFT);

        List<Predicate> wherePredicateList = new ArrayList<>();
        if (terms != null) {
            if (!StringUtils.isEmpty(terms.getFirstName())) {
                wherePredicateList.add(
                        cb.like(cb.lower(archiveTable.get(ArchiveEntry_.FIRST_NAME)),
                                cb.parameter(String.class, "firstName")));
            }
            if (!StringUtils.isEmpty(terms.getLastName())) {
                wherePredicateList.add(
                        cb.like(cb.lower(archiveTable.get(ArchiveEntry_.LAST_NAME)),
                                cb.parameter(String.class, "lastName")));
            }

        }
        query.where(cb.and(wherePredicateList.toArray(new Predicate[wherePredicateList.size()])));
        query.distinct(true);

        TypedQuery<ArchiveEntry> q = em.createQuery(query);

        if (terms != null) {
            if (terms.getFirstName() != null) {
                q.setParameter("firstName", StringUtil.parseString(terms.getFirstName()));
            }
            if (terms.getLastName() != null) {
                q.setParameter("lastName", StringUtil.parseString(terms.getLastName()));
            }
        }

        List<ArchiveEntry> list = q.getResultList();

        for (ArchiveEntry entry : list) {
            ArchiveListItem listItem = new ArchiveListItem();
            listItem.setId(entry.getId());
            listItem.setFirstName(entry.getFirstName());
            listItem.setLastName(entry.getLastName());
            listItem.setMemo(entry.getMemo());
            listItem.setTotalPoints(entry.getTotalPoints());
            listItem.setEvents(new HashSet<ArchiveListItemEvent>());

            for (Event event : entry.getEvents()) {
                ArchiveListItemEvent e = new ArchiveListItemEvent();
                e.setEventName(event.getType().getName());
                e.setPoints(event.getPoints());
                e.setResult(event.getResult().toString());
                e.setTimed(event.getType().isTimed());
                listItem.getEvents().add(e);
            }

            result.getResults().add(listItem);
        }

        return result;
    }
}
