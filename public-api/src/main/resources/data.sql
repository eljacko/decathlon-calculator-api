INSERT INTO public.event_type ("id", "name", "timed") VALUES (1, 'TRACK_100M', true) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (2, 'LONG_JUMP', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (3, 'SHOT_PUT', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (4, 'HIGH_JUMP', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (5, 'TRACK_400M', true) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (6, 'TRACK_110M_HURDLES', true) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (7, 'DISCUS_THROW', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (8, 'POLE_VAULT', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (9, 'JAVELIN_THROW', false) ON CONFLICT DO NOTHING;
INSERT INTO public.event_type ("id", "name", "timed") VALUES (10, 'TRACK_1500M', true) ON CONFLICT DO NOTHING;


