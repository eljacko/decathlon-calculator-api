package com.industry62.decathlon.api.constant;

public final class DecathlonCalculatorApiPaths {

    public static final String CALCULATOR = "/calculator";
    public static final String ARCHIVE = "/archive";
    public static final String ARCHIVE_LIST = "/archive/list";


    private DecathlonCalculatorApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
