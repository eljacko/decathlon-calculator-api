package com.industry62.decathlon.api;

import com.industry62.decathlon.api.util.PubicApiAccessLogReceiver;
import io.undertow.server.HandlerWrapper;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.accesslog.AccessLogHandler;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;
import io.undertow.servlet.api.DeploymentInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.undertow.UndertowDeploymentInfoCustomizer;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
@SuppressWarnings({"checkstyle:FinalParameters", "checkstyle:designforextension"})
public class UndertowConfig {
    @Getter
    @Setter
    @Autowired
    private AccessLogProperties accessLogProperties;

    @Bean
    public UndertowServletWebServerFactory embeddedServletContainerFactory() {
        // @formatter:off
        UndertowServletWebServerFactory factory =
                new UndertowServletWebServerFactory();
        // @formatter:on

        if (getAccessLogProperties().isEnabled()) {
            factory.addDeploymentInfoCustomizers(new UndertowDeploymentInfoCustomizer() {

                @Override
                public void customize(DeploymentInfo deploymentInfo) {
                    deploymentInfo.addInitialHandlerChainWrapper(new HandlerWrapper() {

                        @Override
                        public HttpHandler wrap(HttpHandler handler) {
                            return createAccessLogHandler(handler);
                        }

                    });
                }
            });
        }
        return factory;
    }

    private AccessLogHandler createAccessLogHandler(HttpHandler handler) {
        AccessLogReceiver accessLogReceiver = new PubicApiAccessLogReceiver();
        String formatString;
        if (!StringUtils.isEmpty(getAccessLogProperties().getPattern())) {
            formatString = getAccessLogProperties().getPattern();
        } else {
            formatString = "common";
        }
        return new AccessLogHandler(handler, accessLogReceiver, formatString,
                Thread.currentThread().getContextClassLoader());
    }
}

