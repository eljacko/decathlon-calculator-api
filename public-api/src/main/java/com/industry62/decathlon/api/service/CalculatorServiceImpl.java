package com.industry62.decathlon.api.service;

import com.industry62.decathlon.api.dto.event.EventResultData;
import com.industry62.decathlon.api.dto.event.EventResultsData;
import com.industry62.decathlon.api.dto.response.EventPointsCalculationResult;
import com.industry62.decathlon.api.dto.response.EventsPointsCalculationResult;
import com.industry62.decathlon.datamodel.constant.DecathlonEvent;
import com.industry62.decathlon.datamodel.constant.base.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@SuppressWarnings("checkstyle:designforextension")
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public EventPointsCalculationResult getEventPoints(final String eventName,
                                                       final String result) {
        Event event = DecathlonEvent.getEnum(eventName);
        BigDecimal p = new BigDecimal(result);
        Integer points = event.getPoints(p);
        return new EventPointsCalculationResult(eventName, result, points);
    }

    @Override
    public EventsPointsCalculationResult getEventsPoints(final EventResultsData eventsData) {
        EventsPointsCalculationResult result = new EventsPointsCalculationResult();
        List<EventPointsCalculationResult> eventPoints = new ArrayList<>();
        for (EventResultData ed : eventsData.getEvents()) {
            eventPoints.add(getEventPoints(ed.getEventName(), ed.getResult()));
        }
        result.setEvents(eventPoints);
        return result;
    }
}
