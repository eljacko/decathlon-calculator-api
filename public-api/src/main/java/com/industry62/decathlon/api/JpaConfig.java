package com.industry62.decathlon.api;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.industry62.decathlon.datamodel.repository")
@EntityScan(basePackages = "com.industry62.decathlon.datamodel.entity")
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class JpaConfig {

}
