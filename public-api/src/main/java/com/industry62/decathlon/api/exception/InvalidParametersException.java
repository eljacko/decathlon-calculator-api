package com.industry62.decathlon.api.exception;

import com.industry62.decathlon.api.dto.FieldError;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InvalidParametersException extends RuntimeException {

    private static final long serialVersionUID = 7979151036772999042L;
    private List<FieldError> fieldErrors;

    public InvalidParametersException(final List<FieldError> fieldErrors) {
        super();
        this.fieldErrors = fieldErrors;
    }

}
