package com.industry62.decathlon.api.dto.event;

import com.industry62.decathlon.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class EventResultData {

    @NotNull(message = ValidationMessages.NOT_NULL)
    private String eventName;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private String result;


}
