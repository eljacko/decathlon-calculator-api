package com.industry62.decathlon.api.service;

import com.industry62.decathlon.api.dto.archive.ArchiveData;
import com.industry62.decathlon.api.dto.archive.ArchiveEntryData;
import com.industry62.decathlon.api.dto.event.EventData;
import com.industry62.decathlon.api.dto.event.EventResultData;
import com.industry62.decathlon.api.exception.InvalidParameterException;
import com.industry62.decathlon.datamodel.constant.DecathlonEvent;
import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import com.industry62.decathlon.datamodel.dto.PageResultWrapper;
import com.industry62.decathlon.datamodel.dto.search.ArchiveSearchTerms;
import com.industry62.decathlon.datamodel.entity.ArchiveEntry;
import com.industry62.decathlon.datamodel.entity.event.DiscusThrow;
import com.industry62.decathlon.datamodel.entity.event.Event;
import com.industry62.decathlon.datamodel.entity.event.HighJump;
import com.industry62.decathlon.datamodel.entity.event.JavelinThrow;
import com.industry62.decathlon.datamodel.entity.event.LongJump;
import com.industry62.decathlon.datamodel.entity.event.PoleVault;
import com.industry62.decathlon.datamodel.entity.event.ShotPut;
import com.industry62.decathlon.datamodel.entity.event.Track100m;
import com.industry62.decathlon.datamodel.entity.event.Track110mHurdles;
import com.industry62.decathlon.datamodel.entity.event.Track1500m;
import com.industry62.decathlon.datamodel.entity.event.Track400m;
import com.industry62.decathlon.datamodel.repository.ArchiveRepository;
import com.industry62.decathlon.datamodel.repository.EventRepository;
import com.industry62.decathlon.datamodel.repository.EventTypeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.stream.Collectors;

@Service

@Slf4j
@AllArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class ArchiveServiceImpl implements ArchiveService {
    private ArchiveRepository archiveRepository;
    private EventRepository eventRepository;
    private EventTypeRepository typeRepository;


    @Override
    @Transactional
    public ArchiveData addEntry(final ArchiveEntryData archiveEntryData) {
        Assert.notNull(archiveEntryData, "Archive entry data must not be null");
        ArchiveEntry entryDb = new ArchiveEntry();
        entryDb = archiveRepository.save(entryDb);
        entryDb.setFirstName(archiveEntryData.getFirstName());
        entryDb.setLastName(archiveEntryData.getLastName());
        entryDb.setMemo(archiveEntryData.getMemo());
        entryDb.setEvents(new HashSet<>());

        int totalPoints = 0;
        for (EventResultData e : archiveEntryData.getEvents()) {
            DecathlonEvent event = DecathlonEvent.getEnum(e.getEventName());
            Event eventDb = getDecathlonEventInstance(event);
            eventDb.getType().setName(event.getName());
            eventDb.getType().setTimed(event.isTimedEvent(event.getName()));
            eventDb.setResult(new BigDecimal(e.getResult()));
            int points = event.getPoints(new BigDecimal(e.getResult()));
            totalPoints += points;
            eventDb.setPoints(points);
            eventDb.setArchiveEntry(entryDb);
            eventDb = eventRepository.save(eventDb);
            entryDb.getEvents().add(eventDb);
        }
        entryDb.setTotalPoints(totalPoints);

        ArchiveData archiveData = new ArchiveData();
        archiveData.setId(entryDb.getId());
        archiveData.setFirstName(entryDb.getFirstName());
        archiveData.setLastName(entryDb.getLastName());
        archiveData.setTotalPoints(entryDb.getTotalPoints());
        archiveData.setMemo(entryDb.getMemo());
        archiveData.setEvents(entryDb.getEvents().stream().map(e -> {
            EventData eventData = new EventData();
            eventData.setPoints(e.getPoints());
            eventData.setEventName(e.getType().getName());
            eventData.setResult(e.getResult().toString());
            eventData.setTimed(e.getType().isTimed());
            return eventData;
        }).collect(Collectors.toList()));

        return archiveData;
    }

    @Override
    @Transactional(readOnly = true)
    public PageResultWrapper<ArchiveListItem> getArchiveList(final ArchiveSearchTerms terms) {
        PageResultWrapper<ArchiveListItem> result = archiveRepository.getArchiveList(terms);
        if (result != null && result.getResults() != null) {
            log.debug("Found {} records", result.getResults().size());
        }
        return result;
    }

    @Override
    public Event getDecathlonEventInstance(final DecathlonEvent event) {
        switch (event) {
            case DISCUS_THROW:
                return new DiscusThrow();
            case TRACK_100M:
                return new Track100m();
            case TRACK_400M:
                return new Track400m();
            case TRACK_110M_HURDLES:
                return new Track110mHurdles();
            case TRACK_1500M:
                return new Track1500m();
            case HIGH_JUMP:
                return new HighJump();
            case JAVELIN_THROW:
                return new JavelinThrow();
            case LONG_JUMP:
                return new LongJump();
            case POLE_VAULT:
                return new PoleVault();
            case SHOT_PUT:
                return new ShotPut();
            default:
                throw new InvalidParameterException("eventName", "Unknown event");
        }
    }
}
