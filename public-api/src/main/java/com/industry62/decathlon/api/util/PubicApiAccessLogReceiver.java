package com.industry62.decathlon.api.util;

import com.industry62.decathlon.api.constant.Loggers;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;

public class PubicApiAccessLogReceiver implements AccessLogReceiver {

    @Override
    public final void logMessage(final String message) {
        if (Loggers.ACCESS_LOG_LOGGER.isInfoEnabled()) {
            Loggers.ACCESS_LOG_LOGGER.info(message);
        }
    }
}
