package com.industry62.decathlon.api.dto.archive;

import com.industry62.decathlon.api.dto.event.EventResultData;
import com.industry62.decathlon.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ArchiveEntryData {
    private Long id;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String firstName;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String lastName;
    private String memo;
    @Valid
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Set<EventResultData> events;
}
