package com.industry62.decathlon.api.dto.event;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class EventData {

    private String eventName;
    private String result;
    private Integer points;
    private Boolean timed;

}
