package com.industry62.decathlon.api.service;

import com.industry62.decathlon.api.dto.event.EventResultsData;
import com.industry62.decathlon.api.dto.response.EventPointsCalculationResult;
import com.industry62.decathlon.api.dto.response.EventsPointsCalculationResult;

public interface CalculatorService {

    EventPointsCalculationResult getEventPoints(String eventName, String result);

    EventsPointsCalculationResult getEventsPoints(EventResultsData eventsData);

}
