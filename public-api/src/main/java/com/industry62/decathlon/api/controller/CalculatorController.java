package com.industry62.decathlon.api.controller;

import com.industry62.decathlon.api.constant.DecathlonCalculatorApiPaths;
import com.industry62.decathlon.api.dto.event.EventResultData;
import com.industry62.decathlon.api.dto.event.EventResultsData;
import com.industry62.decathlon.api.dto.response.EventPointsCalculationResult;
import com.industry62.decathlon.api.dto.response.EventsPointsCalculationResult;
import com.industry62.decathlon.api.exception.InvalidParameterException;
import com.industry62.decathlon.api.service.CalculatorService;
import com.industry62.decathlon.datamodel.utils.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings("checkstyle:designforextension")
public class CalculatorController {
    private final CalculatorService calculatorService;

    @GetMapping(DecathlonCalculatorApiPaths.CALCULATOR)
    public HttpEntity<?> calculateEventPoints(
            @RequestParam(value = "eventName", required = true)  final String eventName,
            @RequestParam(value = "result", required = true) final String result) {
        log.info("Request to calculate points for event {} with result {}", eventName, result);
        isResultInRightFormat(result);
        EventPointsCalculationResult response = calculatorService.getEventPoints(eventName, result);
        log.info("Points calculated, sending response: {}", response.toString());
        return new ResponseEntity<EventPointsCalculationResult>(response, HttpStatus.OK);
    }

    @PostMapping(DecathlonCalculatorApiPaths.CALCULATOR)
    public HttpEntity<?> calculateEventsPoints(
            @RequestBody @Valid final EventResultsData eventResultsData) {
        log.info("Request to calculate points for events {}", eventResultsData);
        for (EventResultData ed : eventResultsData.getEvents()) {
            isResultInRightFormat(ed.getResult());
        }
        EventsPointsCalculationResult response =
                calculatorService.getEventsPoints(eventResultsData);
        log.info("Points calculated, sending response: {}", response.toString());
        return new ResponseEntity<EventsPointsCalculationResult>(response, HttpStatus.OK);
    }

    private boolean isResultInRightFormat(final String result) {
        if (StringUtils.isEmpty(result) || !StringUtil.isBigDecimalString(result)) {
            throw new InvalidParameterException("result",
                    "Result required and may contain only numbers and dot character");
        }
        return true;
    }
}
