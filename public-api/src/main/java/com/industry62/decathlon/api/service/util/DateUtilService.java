package com.industry62.decathlon.api.service.util;

import java.sql.Timestamp;
import java.time.Instant;

public interface DateUtilService {

    java.util.Date getDate();

    Timestamp getCurrentTimeAsTimestamp();

    Instant getInstantNow();

}
