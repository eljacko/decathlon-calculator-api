package com.industry62.decathlon.api.service;

import com.industry62.decathlon.api.dto.archive.ArchiveData;
import com.industry62.decathlon.api.dto.archive.ArchiveEntryData;
import com.industry62.decathlon.datamodel.constant.DecathlonEvent;
import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import com.industry62.decathlon.datamodel.dto.PageResultWrapper;
import com.industry62.decathlon.datamodel.dto.search.ArchiveSearchTerms;
import com.industry62.decathlon.datamodel.entity.event.Event;

public interface ArchiveService {

    ArchiveData addEntry(ArchiveEntryData archiveEntryData);

    PageResultWrapper<ArchiveListItem> getArchiveList(ArchiveSearchTerms terms);

    Event getDecathlonEventInstance(DecathlonEvent decathlonEvent);

}
