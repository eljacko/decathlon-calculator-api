package com.industry62.decathlon.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@SuppressWarnings({"checkstyle:designforextension", "checkstyle:HideUtilityClassConstructor"})
public class DecathlonCalculatorApiApplication {


    public DecathlonCalculatorApiApplication() {
        super();
    }

    public static void main(final String[] args) {
        SpringApplication.run(DecathlonCalculatorApiApplication.class, args);
     }
}
