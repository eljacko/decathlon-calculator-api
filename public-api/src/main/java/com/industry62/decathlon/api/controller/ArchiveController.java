package com.industry62.decathlon.api.controller;

import com.industry62.decathlon.api.constant.DecathlonCalculatorApiPaths;
import com.industry62.decathlon.api.dto.FieldError;
import com.industry62.decathlon.api.dto.archive.ArchiveData;
import com.industry62.decathlon.api.dto.archive.ArchiveEntryData;
import com.industry62.decathlon.api.dto.response.ArchiveListResponse;
import com.industry62.decathlon.api.dto.event.EventResultData;
import com.industry62.decathlon.api.exception.InvalidParametersException;
import com.industry62.decathlon.api.service.ArchiveService;
import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import com.industry62.decathlon.datamodel.dto.PageResultWrapper;
import com.industry62.decathlon.datamodel.dto.search.ArchiveSearchTerms;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings({ "checkstyle:magicnumber", "checkstyle:designforextension" })
public class ArchiveController {
    private ArchiveService archiveService;

    @GetMapping(DecathlonCalculatorApiPaths.ARCHIVE_LIST)
    public HttpEntity<?> getList(
            @RequestParam(value = "firstName", required = false) final String firstName,
            @RequestParam(value = "lastName", required = false) final String lastName) {

        log.debug("Request to get archive entries");
        ArchiveSearchTerms terms = new ArchiveSearchTerms();
        if (firstName != null && !StringUtils.isEmpty(firstName)) {
            terms.setFirstName(firstName);
        }
        if (firstName != null && !StringUtils.isEmpty(lastName)) {
            terms.setLastName(lastName);
        }
        PageResultWrapper<ArchiveListItem> result =
                archiveService.getArchiveList(terms);

        ArchiveListResponse response = new ArchiveListResponse();
        response.setRecords(result.getResults());

        log.debug("Found records data {}", response.toString());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(DecathlonCalculatorApiPaths.ARCHIVE)
    public HttpEntity<?> addCalculationSet(
            @RequestBody @Valid final ArchiveEntryData archiveEntryData) {
        log.info("Request to add result set to archive {}", archiveEntryData);
        ArchiveData result = null;
        if (isValidSaveRequestData(archiveEntryData)) {
            result = archiveService.addEntry(archiveEntryData);
        }
        log.info("Added new enty to archive {}", result);
        return new ResponseEntity<ArchiveData>(result, HttpStatus.OK);
    }


    private boolean isValidSaveRequestData(final ArchiveEntryData archiveEntryData) {
        List<FieldError> fieldErrors = new ArrayList<>();
        if (archiveEntryData == null || archiveEntryData.getFirstName() == null) {
            fieldErrors.add(new FieldError("firstName", "First name should not be empty."));
        } else {
            archiveEntryData.setFirstName(archiveEntryData.getFirstName().trim());
            if (StringUtils.isEmpty(archiveEntryData.getFirstName())
                    || archiveEntryData.getFirstName().length() < 2) {
                fieldErrors.add(new FieldError("firstName", "Min length 2 characters"));
            }
        }
        if (archiveEntryData == null || archiveEntryData.getLastName() == null) {
            fieldErrors.add(new FieldError("lastName", "Last name should not be empty."));
        } else {
            archiveEntryData.setLastName(archiveEntryData.getLastName().trim());
            if (StringUtils.isEmpty(archiveEntryData.getLastName())
                    || archiveEntryData.getLastName().length() < 2) {
                fieldErrors.add(new FieldError("lastName", "Min length 2 characters"));
            }
        }
        if (archiveEntryData == null || archiveEntryData.getEvents() == null
                || archiveEntryData.getEvents().isEmpty()) {
            fieldErrors.add(new FieldError("events", "At least one event data is required."));
        } else if (archiveEntryData.getEvents().size() > 10) {
            fieldErrors.add(new FieldError("events", "Decathlon has maximum of 10 events."));
        } else {
            List<String> uniques = new ArrayList<>();
            List<String> duplicates = new ArrayList<>();
            for (EventResultData e : archiveEntryData.getEvents()) {
                if (uniques.contains(e.getEventName())) {
                    duplicates.add(e.getEventName());
                } else {
                    uniques.add(e.getEventName());
                }
            }
            if (!CollectionUtils.isEmpty(duplicates)) {
                fieldErrors.add(new FieldError("events",
                        "Events list contains duplicate results for events"
                                + duplicates.toString()));
            }
        }

        if (CollectionUtils.isEmpty(fieldErrors)) {
            return true;
        }
        throw new InvalidParametersException(fieldErrors);
    }


}
