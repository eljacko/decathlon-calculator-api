package com.industry62.decathlon.api.dto.response;

import com.industry62.decathlon.datamodel.dto.ArchiveListItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ArchiveListResponse {
    private Collection<ArchiveListItem> records;
}
