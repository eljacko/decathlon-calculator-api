package com.industry62.decathlon.api.dto.archive;

import com.industry62.decathlon.api.dto.event.EventData;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ArchiveData {

    private Long id;
    private String firstName;
    private String lastName;
    private Integer totalPoints;
    private String memo;
    private List<EventData> events;

}
